stages:
  - build-images
  - test
  - cleanup
  - publish

include:
  - local: .gitlab-ci/publish.yml

variables:
  TPFS_ANSIBLE_IMAGE_VERSION: "0.17"
  TPFS_ANSIBLE_IMAGE: "gcr.io/xand-dev/tpfs-ansible:${TPFS_ANSIBLE_IMAGE_VERSION}"
  TPFS_ANSIBLE_DEVELOPER_IMAGE: "gcr.io/xand-dev/tpfs-developer-ansible:${TPFS_ANSIBLE_IMAGE_VERSION}"
  COLLECTION_NAMESPACE: "tpfsio"
  REPO_NAME: "ansible"

.trace-job-begin: &trace-job-begin
  # Store the job's start time as a POSIX timestamp in a
  # temporary file for job-level tracing.
  - date +%s | tr -d '\n' | tee "/tmp/${CI_JOB_ID}.start"

.trace-job-end: &trace-job-end
  - trace-job.sh "$(cat /tmp/${CI_JOB_ID}.start)" $CI_JOB_NAME

##
## Build CI Images
##
build-ci-images:
  stage: build-images
  only:
    changes:
      - docker/**/*
  image: docker:19.03
  tags:
    - kubernetes-runner
  services:
    - docker:19.03-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://localhost:2375/
    # 19.03 defaults to enabling TLS
    DOCKER_TLS_CERTDIR: ""
  except:
    - schedules
  interruptible: true
  before_script:
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
    - apk update && apk upgrade && apk add bash # Install bash
    - which bash
    - cd docker
  script:
    - ./build_and_push_ci_images_if_not_exists.sh

##
## Run tests
##
python_tests:
  image: $TPFS_ANSIBLE_DEVELOPER_IMAGE
  tags:
    - kubernetes-runner
  stage: test
  script:
    - ./run_tests.sh

molecule_tests:
  image: $TPFS_ANSIBLE_IMAGE
  tags:
    - kubernetes-runner
  stage: test
  services:
    - docker:19.03-dind
  except:
    - tags
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://localhost:2375/
    # 19.03 defaults to enabling TLS
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
    - echo $GCLOUD_QA_SERVICE_KEY | base64 -id > /tmp/xand-qa.json
    - sed -e s/{{bucket_name}}/"ansible-pipeline-${CI_PIPELINE_ID}-bucket"/g -e s/{{workspace_name}}/"ansible-pipeline-${CI_PIPELINE_ID}-workspace"/g -e s/{{cluster_name}}/"ansible-pipeline-${CI_PIPELINE_ID}-cluster"/g .gitlab-ci/test_vars/gke.yml.template > vars/gke.yml
    - sed -e s/{{loggly_token}}/"${DEV_LOGGLY_TOKEN}"/g .gitlab-ci/test_vars/loggly.yml.template > vars/loggly.yml
    - ESCAPED_ARTIFACTORY_PASS=$(echo $ARTIFACTORY_PASS | sed -e 's/|/\\|/g' -e 's/&/\\&/g')
    - sed -e s/{{artifactory_username}}/"${ARTIFACTORY_USER}"/g -e s/{{artifactory_password}}/"${ESCAPED_ARTIFACTORY_PASS}"/g .gitlab-ci/test_vars/artifactory.yml.template > vars/artifactory.yml
    - cp .gitlab-ci/test_vars/company_name.yml vars
    - cp .gitlab-ci/test_vars/cloud_provider.yml vars
    - cp .gitlab-ci/test_vars/domains.yml vars
    - cp .gitlab-ci/test_vars/namespace.yml vars
    - cp .gitlab-ci/test_vars/tpfs_supplied_values_ci.yml vars
    - export USER=root
    - ansible-galaxy collection install -fr requirements.yml
  script:
    - python3 .gitlab-ci/molecule_tests.py

##
## Cluster deployment testing
##
.cluster-base:
  image: $TPFS_ANSIBLE_IMAGE
  tags:
    - kubernetes-runner
  services:
    - docker:19.03-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://localhost:2375/
    # 19.03 defaults to enabling TLS
    DOCKER_TLS_CERTDIR: ""
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: on_success
    - if: $CI_COMMIT_BRANCH
      when: manual
      allow_failure: true
    - when: never
  before_script:
    - *trace-job-begin
    - export USER=root
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
    - ansible-galaxy collection install -fr requirements.yml
  after_script:
    - *trace-job-end

cluster-gke-deploy:
  extends: .cluster-base
  stage: test
  script:
    - trace-cmd.sh ansible-deploy-gke ansible-playbook -e cloud_provider="google" -e wrapped_playbook="../provision_cluster_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml


cluster-gke-destroy:
  extends: .cluster-base
  stage: cleanup
  dependencies:
    - cluster-gke-deploy
  script:
    - trace-cmd.sh ansible-deploy-gke ansible-playbook -e cloud_provider="google" -e wrapped_playbook="../destroy_all_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml

cluster-eks-deploy:
  extends: .cluster-base
  stage: test
  script:
    # AWS cluster deploy is known to fail from an auth failure. If so, re-run once.
    - trace-cmd.sh ansible-deploy-eks ansible-playbook -e cloud_provider="aws" -e wrapped_playbook="../provision_cluster_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml || ansible-playbook -e cloud_provider="aws" -e wrapped_playbook="../provision_cluster_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml

cluster-eks-destroy:
  extends: .cluster-base
  stage: cleanup
  dependencies:
    - cluster-eks-deploy
  script:
    - trace-cmd.sh ansible-deploy-gke ansible-playbook -e cloud_provider="aws" -e wrapped_playbook="../destroy_all_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml


cluster-aks-deploy:
  extends: .cluster-base
  stage: test
  script:
    - trace-cmd.sh ansible-deploy-aks ansible-playbook -e cloud_provider="azure" -e wrapped_playbook="../provision_cluster_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml

cluster-aks-destroy:
  extends: .cluster-base
  stage: cleanup
  dependencies:
    - cluster-eks-deploy
  script:
    - trace-cmd.sh ansible-deploy-aks ansible-playbook -e cloud_provider="azure" -e wrapped_playbook="../destroy_all_playbook.yml" ci_deploy_testing/cluster_config_wrapping_playbook.yml
