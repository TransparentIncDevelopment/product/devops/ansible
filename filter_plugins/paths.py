from __future__ import (absolute_import, division, print_function)
import os.path
from ansible.errors import AnsibleFilterTypeError
from ansible.module_utils.six import string_types
from ansible.module_utils.common.collections import is_sequence
__metaclass__ = type


def path_join(paths):
    ''' takes a sequence or a string, and return a concatenation
        of the different members '''
    if isinstance(paths, string_types):
        return os.path.join(paths)
    elif is_sequence(paths):
        return os.path.join(*paths)
    else:
        raise AnsibleFilterTypeError("|path_join expects string or sequence, got %s instead." % type(paths))


class FilterModule(object):
    def filters(self):
        return {'path_join': path_join}
