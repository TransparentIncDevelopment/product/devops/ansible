#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from setuptools import setup  # type: ignore

setup(name='ansible_dev_scripts',
      version='0.1',
      author='engineering@tpfs.io',
      packages=["data_types", "sanitization", "vars"],
      install_requires=[])
