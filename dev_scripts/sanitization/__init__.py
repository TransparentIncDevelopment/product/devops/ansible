#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from data_types import PartnerType
import glob
import os
from os.path import isfile, isdir, join
from shutil import rmtree

DEV_ITEMS_TO_REMOVE = [
    ".git",
    ".gitignore",
    ".gitlab-ci",
    ".gitlab-ci.yaml",
    "ansible_collections",
    "ci_deploy_testing",
    "dev_scripts",
    "docker",
    "molecule",
    "multiple_validator_playbook.yml",
    "provision_cluster_playbook.yml",
    "README.md",
    "requirements-development.txt",
    "roles/.gitlab-ci",
    "run_tests.sh",
    "run_tests_playbook.yml",
    "tests",
    "tests_data"
]

# Only keep playbooks that contain the words described below
# for each partner type.
PLAYBOOK_CONTAINS_WORD = {
    PartnerType.Validator: "validator",
    PartnerType.Member: "member",
    PartnerType.Trust: "trust",
    PartnerType.LimitedAgent: "limited_agent",
}


def sanitize_ansible_directory(directory_path: str, partner_type: PartnerType):
    if partner_type not in PLAYBOOK_CONTAINS_WORD:
        raise Exception(f"Partner Type '{partner_type}' not found in PLAYBOOK_CONTAINS_WORD in sanitization.")

    print(f"Sanitizing directory '{directory_path}'")
    _remove_dev_items(directory_path)
    _remove_non_partner_type_playbooks(directory_path, partner_type)
    _remove_molecule_files(directory_path)


def _remove_dev_items(directory_path: str):

    for item in DEV_ITEMS_TO_REMOVE:
        path = join(directory_path, item)

        if isdir(path):
            rmtree(path, ignore_errors=True)

        if isfile(path):
            os.remove(path)


def _remove_non_partner_type_playbooks(directory_path: str, partner_type: PartnerType):
    playbook_glob = join(directory_path, '*_playbook.yml')
    playbooks = glob.glob(playbook_glob)
    playbook_contains_word = PLAYBOOK_CONTAINS_WORD[partner_type]

    for playbook in playbooks:
        # If the word that is expcted to be in the playbook name is not found
        # then remove it or if the destroy word is not in playbook
        if playbook_contains_word not in playbook and "destroy" not in playbook:
            os.remove(playbook)


def _remove_molecule_files(directory_path: str):
    molecule_glob = join(directory_path, "**", "molecule")
    molecule_paths = glob.glob(molecule_glob, recursive=True)

    for molecule_path in molecule_paths:
        rmtree(molecule_path, ignore_errors=True)
