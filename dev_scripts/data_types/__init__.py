#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import enum


class PartnerType(enum.Enum):
    Validator = 1
    Member = 2
    Trust = 3
    LimitedAgent = 4

    def __str__(self):
        return str(self.name)

    @staticmethod
    def from_string(s):
        try:
            return PartnerType[s]
        except KeyError as e:
            raise ValueError(f"{s} not found in PartnerType.") from e
