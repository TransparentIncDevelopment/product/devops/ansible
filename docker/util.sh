# Hacky way to check if fully qualified image exists. User must be authorized for given Container Registry
# This is used because gcr allows overwriting of tags - so we do this check so we can fail CI
# if a job tries to overwrite an existing tagged image.

# Example usage:
# `does_manifest_exist "gcr.io/xand-dev/some-image:some-tag"`
# will print "yes" or "no" to stdout
function does_manifest_exist {
    local OUTPUT;
    local RETCODE;

    # Capture stdout and stderr to confirm if error was "no such manifest"
    OUTPUT=$(DOCKER_CLI_EXPERIMENTAL=enabled docker manifest inspect $1 2>&1 )
    RETCODE=$?
    if [[ $RETCODE -eq 0 ]]
    then
        echo "yes"
        return 0
    else
        if [[ $RETCODE -eq 1 ]] && [[ $OUTPUT =~ "no such manifest" ]]
        then
            echo "no"
            return 0
        fi
    fi
    >&2 echo "Error in checking manifest"
    >&2 echo $OUTPUT
    return 1
}
