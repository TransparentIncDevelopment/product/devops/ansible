#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by humans.
    The purpose of this script is to copy necessary requirements.txt files and then
    run docker build for development purposes. For the script we use in CI, see
    build_and_push_ci_images_if_not_exists.sh
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

# Set /docker path by picking up location of this script
DOCKER_DIR="$( dirname "${BASH_SOURCE[0]}" )"

# Copy requirements.txt and requirements-development.txt from repo root
cp $DOCKER_DIR/../requirements.txt $DOCKER_DIR && cp $DOCKER_DIR/../requirements-development.txt $DOCKER_DIR

docker build -f Dockerfile $DOCKER_DIR
