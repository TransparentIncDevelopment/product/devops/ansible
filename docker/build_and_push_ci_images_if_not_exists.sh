#!/usr/bin/env bash
# Note: This script is used in CI to conditionally build base docker images by comparing the tag
# specified in the neighboring `.env` file, and the tags published in GCR. If a matching tag already
# exists in GCR, this script will continue without building the image.


# Exit if error occurs
set -e

echo "Running script from: $(pwd)"

# Set /docker path by picking up location of this script
DOCKER_DIR="$( dirname "${BASH_SOURCE[0]}" )"

# Copy requirements.txt and requirements-development.txt from repo root
cp $DOCKER_DIR/../requirements.txt $DOCKER_DIR && cp $DOCKER_DIR/../requirements-development.txt $DOCKER_DIR

# Load build env vars and utility functions
source $DOCKER_DIR/.env
source $DOCKER_DIR/util.sh

# tpfs-ansible img ##############################################################
echo "Working on" $TPFS_ANSIBLE_CI_IMG

TPFS_ANSIBLE_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_ANSIBLE_CI_IMG")

if [[ $TPFS_ANSIBLE_CI_IMG_EXISTS == "no" ]];
then
    # Print commands
    set -x
    docker build -t $TPFS_ANSIBLE_CI_IMG -f Dockerfile $DOCKER_DIR
    docker push $TPFS_ANSIBLE_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_ANSIBLE_CI_IMG exists. Continuing..."
fi

# tpfs-developer-ansible img ##############################################################
echo "Working on" $TPFS_ANSIBLE_DEVELOPER_CI_IMG

TPFS_ANSIBLE_DEVELOPER_CI_IMG_EXISTS=$(does_manifest_exist "$TPFS_ANSIBLE_DEVELOPER_CI_IMG")

if [[ $TPFS_ANSIBLE_DEVELOPER_CI_IMG_EXISTS == "no" ]];
then
    cp Dockerfile Dockerfile_developer_edition
    # Remove any notion of python3.6 to python3 which should be latest version of python3.
    sed -i"" -e "s/python:3\.6/python:3/" Dockerfile_developer_edition
    # Print commands
    set -x
    docker build -t $TPFS_ANSIBLE_DEVELOPER_CI_IMG -f Dockerfile_developer_edition $DOCKER_DIR
    docker push $TPFS_ANSIBLE_DEVELOPER_CI_IMG
    # Stop printing commands
    set +x
else
    echo "Image manifest for $TPFS_ANSIBLE_DEVELOPER_CI_IMG exists. Continuing..."
fi
