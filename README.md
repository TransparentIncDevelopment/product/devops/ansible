# Ansible Proof of Concept

This repository represents some implementations, but this isn't finalized and will likely be split up in to different repositories and will need to be combined in some way. So bare with us in this repo as this should just be considered under construction for the time being.

# Project License

MIT OR Apache-2.0

# Installation of Docker

This repository requires a working docker installation. Please install docker as instructed here: https://docs.docker.com/engine/install/

# Installation of Ansible and pip3 packages

For convenience and consistency, a `requirements.txt` file has been provided to install Ansible and the required pip3 packages. Make sure that pip3 is installed by running the following commands:

```shell
sudo apt update
sudo apt install python3-pip
```

Then, you will need to run the following to install Ansible and the associated pip3 packages.
```
sudo ./install_python_prereqs.sh
``` 

## Installation of Community Packages

`ansible-galaxy` is the package manager for ansible, and there is a `requirements.yml` file that describes the ansible package dependencies for this project. After installing `ansible` from above, run the following to install the dependencies, which will be downloaded to the folder: `~/.ansible`.

```shell
ansible-galaxy install -fr requirements.yml
```

## Variables

In order to deploy a cluster and validator using this repository, you will need to set a number of variables. These variables are stored in the folder `ansible/vars`. .template files have been created to demonstrate what variables we expect need to be set. You will want to copy the files with variables you need to .yml files in the same directory and fill them out appropriately. Please be aware that as this repository is under active development the required variables are frequently changing.

## Service Accounts

_For development purposes, we are currently using a shared service account for each cloud. 
These credentials are stored in 1password [here](https://transparentsystems.1password.com/vaults/all/allitems/22nbj5oqubjpb2cqxshqknw3ei). 
It is recommended that testing and development deployments use these shared service accounts rather 
than generating new ones._

Deploying a cluster requires account credentials in the appopriate cloud. These credentials must be added to the appropriate variable for the cloud you are deploying to. 

When deploying to gcloud, a service user must be created with owner role permissions to the appropriate project. For convenience, a script has been included in the /scripts folder called `create_gcloud_service_account.sh` that will create such a service user. When run, it will generate a credentials json file for this service user in the ansible /vars folder, and then generate or update a file called gke.yml in the Ansible /vars folder with the path to this json credentials file.
```shell
./scripts/create_gcloud_service_account.sh
```

When deploying to Azure, a service principal must be created with owner permissions to the target subscription. For convenience, a script has been included in the /scripts folder called `create_azure_service_principal.sh` that will create such a service principal. When run, it will create the service principal, and then generate or update a file called aks.yml in the Ansible /vars folder with the credentials for the service principal.
```shell
./scripts/create_azure_service_principal.sh
```

When deploying to AWS, an IAM user with Owner permission's Access key ID and Secret access key may be used directly. You will need to include them in the eks.yml file before deployment. If you do not currently have an Access key ID and Secret access key, you will need to [generate one using the AWS console](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey).

## Running a Playbook

Once you have installed the dependencies and set the necessary variables, you can run the various playbooks included in the base dir. For example, to provision a cluster, you can run the following:

```shell
ansible-playbook -K single_validator_playbook.yml # Provisions a single validator.
```

These playbooks create a number of resources. For testing purposes, we have also provided a set of playbooks that are designed to tear down resources that were created. These playbooks include:

```shell
ansible-playbook -K destroy_cluster_playbook.yml # Runs terraform destroy on any resources created, and deletes the associated storage bucket.
ansible-playbook -K destroy_all_playbook.yml # This will run terraform destroy, delete the associate storage bucket, delete any toolchain containers that are stopped, and delete the host_save_dir on the host machine. It is intended to fully clean up everything while testing or developing the project.
```

Ansible playbooks are idempotent. That means, to continue on with additional tasks that failed or were unable to complete, you simply need to re-run the playbook. For example, running `single_validator_playbook.yml` without the chainspec information specified in the var files will set up a validator up to the point where it needs the chainspec variables. If you come back and add the chainspec variables to the appropriate /vars/ yaml file, and re-run the playbook, it will continue on to configure the chainspec in your validator.

## Development

To work on ansible roles directly on your machine, you may wish to install the pip3 packages that are normally run in CI. To help with this, we have provided a `requirements-development.txt` file. To install these packages, run:

```shell
sudo pip3 install -r requirements-development.txt
```

## Testing

### Unit Tests for Modules

This repo contains unit tests for custom Ansible Modules. 

To run unit tests and sanity checks (aka linting), execute the `run-tests.sh` bash script:
```bash
./run-tests.sh
```

Ansible's out-of-the-box test utility is the `ansible-test` command line tool. Because of peculiar requirements around
paths it has been encapsulated into the script. The script copies the whole repo into a special sub-folder,
moves the python modules into the plugins folder, and lastly executes **sanity checks** and module **unit tests**.

The `./tests/sanity` folder contains configuration for ignoring some sanity checks. See folder for examples.

For any new ansible module you'll see the following errors:
```shell
ERROR: plugins/modules/my_new_ansible_module.py:0:0: invalid-documentation: DOCUMENTATION.author: Invalid author for dictionary value @ data['author']. Got ['Transparent Financial Systems Engineering (engineering@tfps.io)']
ERROR: plugins/modules/my_new_ansible_module.py:0:0: missing-gplv3-license: GPLv3 license header not found in the first 20 lines of the module
```
Please append to the `./tests/sanity/ignore-$ANSIBLE_VERSSION.txt` the following lines (glob patterns didn't work):
```shell
plugins/modules/my_new_ansible_module.py validate-modules:missing-gplv3-license
plugins/modules/my_new_ansible_module.py validate-modules:invalid-documentation
```

The `./tests/unit` folder contains module unit tests. See folder for examples.

### Molecule

Molecule aids in the development and testing of Ansible roles, particularly against live infrastructure.

Each role supporting molecule testing has a `molecule/` sub-directory.

In our use case, we use molecule to run dependencies (e.g. deploying a cluster) for setup, and then
separately run the "role under test" repeatedly. 

All commands must be run within the role's directory.

Set up a role's dependencies with:
```bash
molecule create
```

Run the current role
```bash
molecule converge
```

Run tests for the current role
```bash
molecule verify
```

Clean up the _current_ role's changes. (Especially useful for a code-a-little-test-a-little loop using `converge`/`cleanup`)
```bash
molecule cleanup
```

Clean up the current role's dependencies (e.g. delete the cluster)
```bash
molecule destroy
```

#### Set up for molecule testing

##### Cluster

Create a `.yml` file for each of these `.template` files and fill in the relevant fields:

- `./vars/user_supplied_values.yml.template`
- `./vars/tpfs_supplied_values.yml.template`


Reference the template file for the cloud of your choice to create its var file

- Azure -> `./vars/aks.yml.template`
- GCP -> `./vars/gke.yml.template`
- AWS -> `./vars/eks/yml.template`

### Deploying an empty cluster

An empty cluster can be useful for testing k8s definitions in other repositories. To deploy an empty
cluster:
1. Fill out the files specified directly above in [Set up for molecule testing](#set-up-for-molecule-testing). 
**Google** is recommended as the cloud provider since it is quick to deploy, and our main cloud provider.


2. Run the playbook for provisioning an empty cluster:  
`ansible-playbook -K provision_cluster_playbook.yml`


3. Configure `kubectl` to point to your new cluster. If using gcloud, login to gcloud console, navigate
to your gke cluster, and copy the connection string. Something like this:  
`gcloud container clusters get-credentials my-cluster --region=us-west2`


4. Please delete your cluster when you are done since it costs $$$ to keep around:  
`ansible-playbook -K destroy_all_playbook.yml`

## Troubleshooting Notes

To see more diagnostic detail when running `ansible-playbook` use the flags `-v`, `-vv`, or `-vvv`.
