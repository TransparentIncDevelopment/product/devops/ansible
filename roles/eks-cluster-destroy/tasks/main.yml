- name: Re-run terraform to retrieve kubernetes configuration file.
  community.general.terraform:
    project_path: "{{ base_save_dir }}/terraform-eks"
    workspace: "{{ t4_workspace }}"
    state: present
    force_init: yes
    init_reconfigure: yes
    backend_config:
      bucket: "{{ eks.resource_name_prefix }}{{ eks.s3_bucket }}"
      region: "{{ eks.region }}"
      dynamodb_table: "{{ eks.resource_name_prefix }}{{ eks.s3_bucket }}"
    variables:
      cluster_name: "{{ eks.resource_name_prefix }}{{ eks.cluster_name }}"
      cluster_version: "{{ kubernetes_cluster_version }}"
      aws_ssh_keypair_name: "{{ keypair_name }}"
      aws_region: "{{ eks.region }}"
      aws_instance_type: "m5.2xlarge"
      aws_scaling_min_size: "{{ eks.scaling_min_size }}"
      aws_scaling_max_size: "{{ eks.scaling_max_size }}"

# Next sections before terraform destroy deals with deleting Load Balancers created from kubernetes
# since AWS Load Balancer Controller appears to not delete the corresponding infrastructure after
# deleting the service. (Might alternatively been able to be solved via installing the ALB controller)
# Attempted using ansible to connect to the k8s API to delete the services, and using `kubectl delete service`.
# From bug where destroy fails: ADO #6219

# Retrieve All AWS Service so that we get all the load balancer services
- name: Gather All AWS Kubernetes Services
  community.kubernetes.k8s_info:
    api_version: v1
    kind: Service
  register: all_k8s_services

- vars:
    load_balancer_service_query: "resources[?spec.type=='LoadBalancer']"
  set_fact:
    load_balancer_service_query: "{{ load_balancer_service_query }}"
    load_balancer_services: "{{ all_k8s_services | json_query(load_balancer_service_query) }}"

- vars:
    load_balancer_hostnames_query: "status.loadBalancer.ingress[*].hostname"
    # a379c760224f74e0e8c2f549cc1e6c8c-1937340420.us-west-1.elb.amazonaws.com
    # Uses at least 30 hex characters [a-f0-9]{30,} then some other characters and ends in .elb.amazonaws.com
    load_balancer_name_regex: '^([a-f0-9]{30,})-.*\.amazonaws\.com$'
  set_fact:
    load_balancer_names: "{{ load_balancer_services | map('json_query', load_balancer_hostnames_query) | flatten | map('regex_replace', load_balancer_name_regex, '\\1') }}"

- name: Remove All AWS Load Balancer Kubernetes Services
  community.kubernetes.k8s:
    state: absent
    definition: "{{ item }}"
  loop: "{{ load_balancer_services }}"

- name: Retrieve All AWS Kubernetes Load Balancer Services to Verify
  community.kubernetes.k8s_info:
    api_version: v1
    kind: Service
  register: all_verify_k8s_services
  until: all_verify_k8s_services | json_query(load_balancer_service_query) | length == 0
  retries: 30
  delay: 10

- name: Check that they've been removed
  assert:
    that: all_verify_k8s_services | json_query(load_balancer_service_query) | length == 0

- name: Remove All AWS Kubernetes Network ELBs
  community.aws.elb_network_lb:
    name: "{{ item }}"
    region: "{{ eks.region }}"
    state: absent
  loop: "{{ load_balancer_names }}"

# It has been known to create them as classic elbs if network is unavailble
- name: Remove All AWS Kubernetes Classic ELBs
  community.aws.elb_classic_lb:
    name: "{{ item }}"
    region: "{{ eks.region }}"
    state: absent
  loop: "{{ load_balancer_names }}"

# It's not likely that it's including Application ELBs, but just in case
# it's only a bit of execution time to make sure Application ELBs don't exist
- name: Remove All AWS Kubernetes Application ELBs
  community.aws.elb_application_lb:
    name: "{{ item }}"
    region: "{{ eks.region }}"
    state: absent
  loop: "{{ load_balancer_names }}"

- name: Remove All AWS Kubernetes ELB Security Groups
  amazon.aws.ec2_group:
    name: "k8s-elb-{{ item }}"
    region: "{{ eks.region }}"
    state: absent
  loop: "{{ load_balancer_names }}"

# end of fix for ADO #6219

- name: Run Terraform Destroy
  community.general.terraform:
    project_path: "{{ base_save_dir }}/terraform-eks"
    workspace: "{{ t4_workspace }}"
    force_init: yes
    init_reconfigure: yes
    backend_config:
      bucket: "{{ eks.resource_name_prefix }}{{ eks.s3_bucket }}"
      region: "{{ eks.region }}"
      dynamodb_table: "{{ eks.resource_name_prefix }}{{ eks.s3_bucket }}"
    state: absent
    variables:
      cluster_name: "{{ eks.resource_name_prefix }}{{ eks.cluster_name }}"
      cluster_version: "{{ kubernetes_cluster_version }}"
      aws_ssh_keypair_name: "{{ keypair_name }}"
      aws_region: "{{ eks.region }}"
      aws_instance_type: "m5.2xlarge"
      aws_scaling_min_size: "{{ eks.scaling_min_size }}"
      aws_scaling_max_size: "{{ eks.scaling_max_size }}"

# EKS does not delete pvc disks upon cluster deletion. This will find and delete them.

  ### below we set a fact for the key used for the filter, as otherwise
  ### Ansible does not allow key substition in dictionaries. See
  ### https://github.com/ansible/ansible/issues/17324 and
  ### https://stackoverflow.com/questions/38143647/set-fact-with-dynamic-key-name-in-ansible

- name: Set tag needed to find persistent disks
  set_fact:
    aws_volume_tag={"tag:kubernetes.io/cluster/{{ eks.resource_name_prefix }}{{ eks.cluster_name }}":"owned"}

# EKS applies this tag to all persistent disks.
# See: https://docs.aws.amazon.com/eks/latest/userguide/eks-ug.pdf
# for examples for how AWS constructs resource tags.
# The tagging for volumes is the same as the tagging scheme for cluster-autoscalers.

- name: Find all persistent disks created by the cluster {{ eks.resource_name_prefix }}{{ eks.cluster_name }}
  amazon.aws.ec2_vol_info:
    region: "{{ eks.region }}"
    filters:
      "{{ aws_volume_tag }}"
    aws_access_key: "{{ eks.access_key_id }}"
    aws_secret_key: "{{ eks.secret_access_key }}"
  register: pv_disks

- name: Deleting persistent disks from cluster {{ eks.resource_name_prefix }}{{ eks.cluster_name }}
  command:
    cmd: "aws ec2 delete-volume --volume-id {{ item }}"
  with_items: "{{ pv_disks.volumes | map(attribute='id') | list }}"
