---
- name: Create directory for NGINX Ingress Controller files
  file:
    path: "{{ nginx_dir }}"
    state: directory

- name: Retrieve NGINX Ingress Controller Deployment Artifacts
  get_url:
    url: "{{ artifacts_url }}/{{ nginx_repo }}/{{ nginx_package }}"
    dest: "/tmp/{{ nginx_package }}"
    username: "{{ artifactory_username }}"
    password: "{{ artifactory_password }}"

- name: Unzip NGINX Ingress Controller Deployment Artifacts
  unarchive:
    src: "/tmp/{{ nginx_package }}"
    dest: "{{ nginx_dir }}"
    remote_src: yes
    mode: 0700

- name: Kustomize Build NGINX Ingress Controller
  kustomize_build:
    kustomization_dir: "{{ nginx_dir }}"
  register: kustomize_build_nginx

- name: Kubectl apply NGINX Ingress Controller k8s definition
  community.kubernetes.k8s:
    apply: yes
    resource_definition: "{{ kustomize_build_nginx.stdout }}"
  environment:
    KUBECONFIG: "{{ remote_kubeconfig_path }}"
  when: ((dry_run is not defined) or (dry_run == false)) and kustomize_build_nginx.stdout is defined

- name: Get the NGINX Ingress Controller Object
  community.kubernetes.k8s_info:
    api_version: v1
    kind: Service
    namespace: ingress-nginx
    name: nginx-service
  environment:
    KUBECONFIG: "{{ remote_kubeconfig_path }}"
  register: ingress_controller
  until: ingress_controller.resources[0].status.loadBalancer.ingress is defined
  retries: 30
  delay: 10

- name: Output From Execution
  set_fact:
    ingress_ip_or_hostname: "{{ ingress_controller.resources[0].status.loadBalancer.ingress[0] }}"

- name: Save to Disk Whitelister IP/Hostname
  copy:
    content: "{{ ingress_ip_or_hostname | to_nice_json }}"
    dest: "{{ (facts_dir, 'ingress_ip_or_hostname.fact') | path_join }}"
