- name: Create directory for member-api-k8s files
  file:
    path: "{{ member_api_dir }}"
    state: directory

# download and unzip member api k8s from artifactory
- name: Retrieve member-api-k8s Deployment Artifacts
  get_url:
    url: "{{ artifacts_url }}/{{ member_api_repo }}/{{ member_api_package }}"
    dest: "/tmp/{{ member_api_package }}"
    username: "{{ artifactory_username }}"
    password: "{{ artifactory_password }}"

# unzip member api k8s from artifactory
- name: Unzip Member API Deployment Artifacts
  unarchive:
    src: "/tmp/{{ member_api_package }}"
    dest: "{{ member_api_dir }}"
    remote_src: yes
    mode: 0700

# generate jwt
- name: Generate Member API JWT Secret
  set_fact:
    # Not including punctuation characters that can have issues in the console '-,.,!' ADO #6227
    member_api_jwt_secret: "{{ lookup('password', host_xand_dir + '/ansible/passwd.d/member_api_jwt_secret chars=ascii_letters,digits length=64') }}"

- name: Create Wallet JWT
  create_jwt:
    secret: "{{ member_api_jwt_secret }}"
    claims:
      - "iss=xand"
      - "sub=client"
      - "company={{ company_name }}"
  register: jwt_token_wallet

- name: Create Member API Kubernetes Configuration Files
  copy:
    content:  "{{ lookup('template', 'config.yaml.j2') }}"
    dest: "{{ member_api_dir }}/base/config/config.yaml"

- name: Create Member API Kubernetes Secrets Files
  copy:
    content:  "{{ lookup('template', 'secrets.yaml.j2') }}"
    dest: "{{ member_api_dir }}/base/secrets/secrets.yaml"

- name: Run script to configure kubernetes
  command:
    cmd: "./configure-kubernetes.sh -d {{ member_api_domain_name }} -i {{ docker_registry_base }}/member-api -v {{ member_api_version }} -n {{ namespace }} -l overlays/default"
    chdir: "{{ member_api_dir }}"

# kubectl apply
- name: Get xand.yaml
  slurp:
    src: "{{ remote_save_dir }}/member-api-k8s/xand.yaml"
  register: xand_yaml

- name: Kubectl apply member api k8s definition
  community.kubernetes.k8s:
    apply: yes
    resource_definition: "{{ xand_yaml['content'] | b64decode }}"
  environment:
    KUBECONFIG: "{{ remote_kubeconfig_path }}"
  when: ((dry_run is not defined) or (dry_run == false))
