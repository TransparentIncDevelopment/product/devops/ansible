#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: generate_validator_keys

short_description: Generates Keys for the Validator Set up.

version_added: "1.0.0"

description:
    - "Uses xkeygen to generate the authority key, session producing block key, and session finalizing block key."

options:
    facts_dir:
        description:
            - The directory to read and write facts to.
        type: path
        required: true
    xand_dir:
        description:
            - The location to output the keys generated from this task.
        type: path
        default: '/root/xand/consensus-node'
        required: false

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
# Pass in just the name of the node being stood up.
- name: Generate Validator Substrate Keys
  generate_validator_keys:
    facts_dir: ~/xand/ansible/facts.d


# Pass in a message and have changed true
- name: Generate Validator Substrate Keys
  generate_validator_keys:
    facts_dir: "{{ facts_dir }}"
    xand_dir: ~/xand/consensus-node
'''

RETURN = '''
consensus_keys:
    description: Public keys for validator
    type: complex
    returned: always
    contains:
        session_produce_blocks_public_key:
            description: The public key base58 encoded of the asymmetric aura key for producing blocks in substrate.
            type: str
            returned: always
        session_produce_blocks_private_mnemonic:
            description: The private key stored as a bip39 mnemonics of the asymmetric aura key for producing blocks in substrate.
            type: str
            returned: always
        session_finalize_blocks_public_key:
            description: The public key base58 encoded of the asymmetric grandpa key for finalizing blocks in substrate.
            type: str
            returned: always
        session_finalize_blocks_private_mnemonic:
            description: The private key stored as a bip39 mnemonics of the asymmetric grandpa key for finalizing blocks in substrate.
            type: str
            returned: always
        authority_address:
            description: A substrate address which happens to be base58 of the authority key used for signing transactions on substrate.
            type: str
            returned: always
'''

BASE58_CHARACTER_CLASS = "[1-9A-HJ-NP-Za-km-z]"

from ansible.module_utils.basic import AnsibleModule  # type: ignore
import json
import os
import re
__metaclass__ = type


def _xkeygen_generate(module, xkeygen, keyname, descriptive_keyname):
    (return_code, key_output, error) = module.run_command(
        [xkeygen, "generate", keyname],
        check_rc=True,
        cwd=module.params['xand_dir']
    )

    match = re.search(f'{BASE58_CHARACTER_CLASS}{{48}}', key_output)
    if match:
        return match.group(0)
    else:
        raise KeyError(f'Not able to find the {descriptive_keyname} address in the output of xkeygen.')


def _completed_key_generation(consensus_keys):
    return 'session_produce_blocks_public_key' in consensus_keys and \
        'session_produce_blocks_private_mnemonic' in consensus_keys and \
        'session_finalize_blocks_public_key' in consensus_keys and \
        'session_finalize_blocks_private_mnemonic' in consensus_keys and \
        'authority_address' in consensus_keys


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        facts_dir=dict(type='path', required=True),
        xand_dir=dict(type='path', required=False, default='/root/xand/consensus-node')
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        consensus_keys=dict(
            session_produce_blocks_public_key='',
            session_finalize_blocks_public_key='',
            authority_address=''
        )
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Automatically calls module.fail_json if the binary is not found.
    xkeygen = module.get_bin_path("xkeygen", required=True)

    # Using DataLoader as it will be helpful later if used with ansible
    # vault.
    consensus_keys_path = os.path.join(module.params['facts_dir'], "consensus_keys.fact")

    # Loads the consensus_keys fact in case this has run in the past.
    if os.path.exists(consensus_keys_path):
        with open(consensus_keys_path, 'r') as consensus_keys_file:
            result['consensus_keys'] = json.load(consensus_keys_file)

        # The keys already exist so no need to generate new ones just pass the results back
        if _completed_key_generation(result['consensus_keys']):
            module.exit_json(**result)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    try:
        result['consensus_keys']['session_produce_blocks_public_key'] = \
            _xkeygen_generate(module, xkeygen, "validator-session-produce-blocks", "session produce blocks")

        session_produce_blocks_path = os.path.join(module.params['xand_dir'], "validator-session-produce-blocks")
        with open(session_produce_blocks_path, 'r') as session_produce_blocks_file:
            result['consensus_keys']['session_produce_blocks_private_mnemonic'] = \
                session_produce_blocks_file.read().strip()

        result['consensus_keys']['session_finalize_blocks_public_key'] = \
            _xkeygen_generate(module, xkeygen, "validator-session-finalize-blocks", "session finalize blocks")

        session_finalize_blocks_path = os.path.join(module.params['xand_dir'], "validator-session-finalize-blocks")
        with open(session_finalize_blocks_path, 'r') as session_finalize_blocks_file:
            result['consensus_keys']['session_finalize_blocks_private_mnemonic'] = \
                session_finalize_blocks_file.read().strip()

        result['consensus_keys']['authority_address'] = \
            _xkeygen_generate(module, xkeygen, "validator-authority", "authority key")

    except Exception as e:
        module.fail_json(e.message, **result)

    with open(consensus_keys_path, 'w') as consensus_keys_file:
        consensus_keys_file.write(module.jsonify(result['consensus_keys']))

    # We will be creating keys and so changed will be true.
    result['changed'] = True

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
