#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: generate_encryption_key

short_description: Generates an Encryption Key for use of encrypting data on the chain.

version_added: "1.0.0"

description:
    - "Uses xkeygen to generate the encryption key."

options:
    facts_dir:
        description:
            - The directory to read and write facts to.
        required: true
        type: path
    xand_dir:
        description:
            - The location to output the keys generated from this task.
        required: true
        type: path

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
- name: Generate Encryption Key
  generate_encryption_key:
    facts_dir: /root/xand/ansible/facts.d/
    xand_dir: /root/xand/non-consensus-node
'''

RETURN = '''
encryption_key:
    description: Public Address for an Encryption Key
    type: complex
    returned: success
    contains:
        public_key:
            description: The public address of the encryption key.
            type: str
            returned: success
'''

BASE58_CHARACTER_CLASS = "[1-9A-HJ-NP-Za-km-z]"

import json
import os
import re
from ansible.module_utils.basic import AnsibleModule  # type: ignore
__metaclass__ = type


def _xkeygen_generate(module, xkeygen) -> str:
    (return_code, key_output, error) = module.run_command(
        [xkeygen, "generate", "encryption"],
        check_rc=True,
        cwd=module.params['xand_dir']
    )

    match = re.search(f'kp-({BASE58_CHARACTER_CLASS}+)', key_output)
    if match:
        return match.group(1)
    else:
        raise KeyError('Not able to find the encryption public key in the output of xkeygen.')


def _completed_key_generation(encryption_key):
    return 'public_key' in encryption_key


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        facts_dir=dict(type='path', required=True),
        xand_dir=dict(type='path', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        encryption_key=dict(
            public_key=''
        )
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Automatically calls module.fail_json if the binary is not found.
    xkeygen = module.get_bin_path("xkeygen", required=True)

    # Using DataLoader as it will be helpful later if used with ansible
    # vault.
    encryption_key_path = os.path.join(module.params['facts_dir'], "encryption_key.fact")

    # Loads the encryption_key fact in case this has run in the past.
    if os.path.exists(encryption_key_path):
        with open(encryption_key_path, 'r') as encryption_key_file:
            result['encryption_key'] = json.load(encryption_key_file)

        # The keys already exist so no need to generate new ones just pass the results back
        if _completed_key_generation(result['encryption_key']):
            module.exit_json(**result)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    try:
        result['encryption_key']['public_key'] = \
            _xkeygen_generate(module, xkeygen)

    except Exception as e:
        module.fail_json(e.message, **result)

    with open(encryption_key_path, 'w') as encryption_key_file:
        encryption_key_file.write(module.jsonify(result['encryption_key']))

    # We will be creating keys and so changed will be true.
    result['changed'] = True

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
