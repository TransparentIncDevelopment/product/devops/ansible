#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: create_jwt

short_description: Performs create jwt command to run via ansible.

version_added: "1.0.0"

description:
    - "Uses jwtcli to create a jwt using a secret or jwks and outputs the build as a string."

options:
    jwks_path:
        description:
            - The path to the jwks file to use to generate a jwt. Either this or the secret are required.
        type: path
        required: false
    secret:
        description:
            - The secret string used to generate a jwt. Either this or the jwks_path are required.
        type: str
        required: false
    claims:
        description:
            - The array of claims to include when creating the jwt. Format would be expected to be a key value pair 'company=tpfs'.
        type: list
        required: false
        elements: str

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
# Specifies the kustomize build for a consensus or non-consensus xand-node.
- name: Create Network Voting JWT
  create_jwt:
    secret: "09414ab7-69a0-4bd2-a415-beff0a4f5c90"
    claims:
      - iss=xand
      - sub=client
      - company=tpfs
  register: network_voting_cli

- name: Create Promtheus JWT
  create_jwt:
    jwks_path: "/root/xand/prometheus/k8s/partner/jwks"
    claims:
      - iss=xand
      - sub=client
      - company=tpfs
'''

RETURN = '''
create_jwt:
    description: JWT
    type: str
    returned: success
'''

from ansible.module_utils.basic import AnsibleModule  # type: ignore
from typing import Dict, Tuple
__metaclass__ = type


class CreateJWTModule(object):

    # define available arguments/parameters a user can pass to the module
    @staticmethod
    def _argspec() -> Dict:
        argument_spec = dict(
            jwks_path=dict(type='path', required=False),
            secret=dict(type='str', required=False),
            claims=dict(type='list', required=False, elements='str')
        )

        return argument_spec

    def __init__(self, module):
        self.module = module

    def run_jwtcli_jwt(self) -> Tuple[int, str, str]:
        jwks_path = self.module.params["jwks_path"] if self.module.params["jwks_path"] and not self.module.params["jwks_path"].isspace() else None
        secret = self.module.params["secret"] if self.module.params["secret"] and not self.module.params["secret"].isspace() else None
        claims = self.module.params["claims"]

        jwtcli = self.module.get_bin_path("jwtcli", required=True)
        args = [jwtcli, "jwt"]

        if jwks_path:
            args.extend(["--jwks", jwks_path])
        elif secret:
            args.extend(["--secret", secret])

        for claim in claims:
            args.extend(["--claims", claim])

        return self.module.run_command(
            args,
            check_rc=True
        )


def main():
    mutually_exclusive = [
        ("jwks_path", "secret")
    ]

    module = AnsibleModule(
        argument_spec=CreateJWTModule._argspec(),
        mutually_exclusive=mutually_exclusive,
        supports_check_mode=True
    )

    create_jwt = CreateJWTModule(module)

    result = dict(
        changed=False
    )

    (result["rc"], result["jwt"], result["stderr"]) = create_jwt.run_jwtcli_jwt()

    module.exit_json(**result)


if __name__ == '__main__':
    main()
