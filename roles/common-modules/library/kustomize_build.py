#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: kustomize_build

short_description: Performs kustomize build commands to run during a deployment.

version_added: "1.0.0"

description:
    - "Uses kustomize executable to build kubernetes definition(s) and outputs the build as a string."

options:
    kustomization_dir:
        description:
            - The directory where the kustomization.yaml file exists that will be built.
        type: path
        required: true

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
# Specifies the kustomize build for a consensus or non-consensus xand-node.
- name: Build Xand Node Kustomize
  kustomize_build:
    kustomization_dir: /path/to/my/kustomize
  register: xand-k8s

- name: Apply Xand Node K8S Objects
  community.kubernetes.k8s:
    state: present
    definition: "{{ xand-k8s.stdout | from_yaml }}"

# Waits for all validators in the given namespace though there could be other
# deployments of validators that will be checked as well.
- name: Wait for all Applied Xand Node Deployments
  community.kubernetes.k8s_info:
    kind: Deployment
    label_selectors:
      - tier = validator
    namespace: xand
    wait: yes
    wait_condition:
      type: Available
      status: "True"
      reason: MinimumReplicasAvailable
    wait_timeout: 300
'''

RETURN = '''
kustomize_build:
    description: results of kustomize build
    type: complex
    returned: always
    contains:
        rc:
            description: The return code from the kustomize run.
            type: int
            returned: always
        stdout:
            description: Kubernetes definitions that is output from the kustomize build command.
            type: str
            returned: always
        stderr:
            description: The stderr that might have come from the kustomize build execution.
            type: str
            returned: always
'''

from ansible.module_utils.basic import AnsibleModule  # type: ignore
import json
import os
import re
__metaclass__ = type


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        kustomization_dir=dict(type='path', required=True),
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        stdout=""
    )

    # Automatically calls module.fail_json if the binary is not found.
    kustomize = module.get_bin_path("kustomize", required=True)

    (result["rc"], result["stdout"], result["stderr"]) = module.run_command(
        [kustomize, "build", "."],
        check_rc=True,
        cwd=module.params['kustomization_dir']
    )

    # No alterations to the file system occurred here.
    result['changed'] = False

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
