---
- name: Verify
  hosts: molecule-toolchain
  gather_facts: false
  vars:
    k8s_root_dir: "{{ remote_save_dir }}/trust-k8s/trustee"
  tasks:
  - name: Import Variables via Tasks
    include_tasks:
      file: ../../../../import_vars.yml

  # Assert directory created
  - name: Get trust k8s dir stats
    stat:
      path: "{{ k8s_root_dir }}"
    register: trust_stats

  - name: Check trust k8s directory exists
    assert:
      that:
        - trust_stats.stat.exists == true
        - trust_stats.stat.isdir == true

  # Tests for config values coming into place
  - name: Get dev.yaml file contents
    slurp:
      src: "{{ k8s_root_dir }}/config/base/dev.yaml"
    register: dev_yaml_slurp

  - name: Get banks.yaml file contents
    slurp:
      src: "{{ k8s_root_dir }}/config/base/banks.yaml"
    register: banks_yaml_slurp

  - name: Get account-numbers.yaml file contents
    slurp:
      src: "{{ k8s_root_dir }}/config/secrets/account-numbers.yaml"
    register: account_numbers_yaml_slurp

  - name: Get secrets_yaml_contents file contents
    slurp:
      src: "{{ k8s_root_dir }}/config/secrets/secrets.yaml"
    register: secrets_yaml_slurp

  # Assert dev.yaml updates
  - name: Assert expected updates to dev.yaml
    vars:
      dev_yaml_contents: "{{dev_yaml_slurp.content | b64decode | from_yaml }}"
      xand_api_endpoint: "{{ dev_yaml_contents.xand_api_endpoint }}"
      trust_address: "{{ dev_yaml_contents.trust_address }}"
      xand_api_jwt: "{{ dev_yaml_contents.xand_api_jwt }}"
      xand_api_jwt_secret_key: "{{dev_yaml_contents.xand_api_jwt_secret_key}}"
    assert:
      fail_msg: "dev_yaml_contents: {{ dev_yaml_contents }}"
      that:
        - xand_api_endpoint == "http://validators:10044"
        - trust_address == "5D4waPtLP8tLvqJEVACNmyZWNb82MdCjk39kvFAAre1nC8sG"
        - xand_api_jwt_secret_key == "xand-api-jwt-token"

  # Assert banks.yaml updates
  - set_fact:
      banks_yaml_contents: "{{banks_yaml_slurp.content | b64decode | from_yaml }}"

  - name: Assert expected updates to banks.yaml
    vars:
      mcb_bank_cfg: "{{ banks_yaml_contents.banks[0].mcb }}"
      provident_bank_cfg: "{{ banks_yaml_contents.banks[1].treasury_prime }}"
      partner_accounts: "{{ banks_yaml_contents.partner_accounts }}"
    assert:
      fail_msg: |
        banks_yaml_contents: {{ banks_yaml_contents}}
      that:
        - mcb_bank_cfg.bank_api_url == "https://invalid-url.xand.to/metropolitan"
        - mcb_bank_cfg.routing_number == "121141343"
        - mcb_bank_cfg.display_name == "MCB"
        - provident_bank_cfg.bank_api_url == "https://invalid-url.xand.to/provident"
        - provident_bank_cfg.routing_number == "211374020"
        - provident_bank_cfg.display_name == "Provident"

  - name: Assert expected whitelist updates to banks.yaml
    vars:
      members_to_accounts: "{{ banks_yaml_contents.partner_accounts.members_to_accounts }}"
      expected_members_to_accounts: "{{ member_bank_account_whitelist }}"
      member_0: "{{ members_to_accounts['member-0'] }}"
    assert:
      fail_msg: "Found {{ members_to_accounts }} \n\nExpected {{ expected_members_to_accounts }}"
      that:
        # Smoke test member-0
        - member_0[0].account_number == '2000000'
        - member_0[0].routing_number == '121141343'
        # Assert entire whitelist
        - members_to_accounts == expected_members_to_accounts

    # Assert updates to account-numbers.yaml
  - set_fact:
      account_numbers_yaml_content: "{{account_numbers_yaml_slurp.content | b64decode | from_yaml }}"

  - name: Assert expected trust accounts in account-numbers.yaml
    vars:
      mcb: "{{ account_numbers_yaml_content.banks.mcb }}"
      provident: "{{ account_numbers_yaml_content.banks.provident }}"
    assert:
      fail_msg: "Found {{ account_numbers_yaml_content }}"
      that:
        - mcb.trust_account == '1234567'
        - provident.trust_account == '7654321'

  # Assert updates to secrets.yaml file
  - set_fact:
      secrets: "{{secrets_yaml_slurp.content | b64decode | from_yaml }}"

  - name: Assert contents of secrets.yaml file
    assert:
      that:
        # All expected values are found sibling molecule.yml file
        - secrets['mcb-client-app-ident'] == "API_TEST"
        - secrets['mcb-organization-id'] == "12345"
        - secrets['mcb-username'] == "molecule-username-mcb"
        - secrets['mcb-password'] == "molecule-password-mcb"
        - secrets['provident-api-key-id'] == "key_id"
        - secrets['provident-api-key-value'] == "key_value"
        - secrets['xand-api-jwt-token'] == xand_api_jwt_token.jwt

  # Test for kustomize edit/set/build
  - name: Get kustomize config file
    slurp:
      src: "{{ k8s_root_dir }}/kustomization.yaml"
    register: kustomize_config_content

  - name: Assert image names set in kustomization.yaml
    vars:
      kustomize_config: "{{ kustomize_config_content.content | b64decode | from_yaml}}"
      set_trust_image: "{{ kustomize_config | json_query('images[?name==`trust`]') | first }}"
      set_prometheus_pipesrv_image: "{{ kustomize_config | json_query('images[?name==`prometheus-pipesrv`]') | first }}"
    assert:
      that:
        - set_trust_image.newName == "{{ docker_registry_base }}/trust"
        - set_trust_image.newTag == trust_image_version
        - set_prometheus_pipesrv_image.newName == "{{ docker_registry_base }}/prometheus-pipesrv"
        - set_prometheus_pipesrv_image.newTag == prometheus_pipe_srv_version

  - name: Assert namespace set in kustomization.yaml
    vars:
      kustomize_config: "{{ kustomize_config_content.content | b64decode | from_yaml}}"
    assert:
      fail_msg: "Expected {{ namespace }}; Found {{ kustomize_config.namespace }}"
      that:
        - kustomize_config.namespace == namespace

  - name: Stat kustomize build output file
    stat:
      path: "{{ k8s_root_dir }}/application.yaml"
    register: kustomize_build_file_stats

  - name: Assert kustomize build file exists
    assert:
      that:
        - kustomize_build_file_stats.stat.exists == true

  - name: Assert kustomize build is applyable
    shell:
      chdir: "{{ k8s_root_dir }}"
      cmd: "kubectl apply -f {{ k8s_root_dir }}/application.yaml --dry-run=client"

  # Fetch trustee's Deployment and assert it is healthy
  - name: Assert Expected Replica Count
    community.kubernetes.k8s_info:
      api_version: apps/v1
      kind: Deployment
      namespace: "{{ namespace }}"
      name: trustee
    environment:
      KUBECONFIG: "{{ remote_kubeconfig_path }}"
    register: trustee_deployment
    until: trustee_deployment.resources[0].status.readyReplicas is defined and trustee_deployment.resources[0].status.readyReplicas == trustee_deployment.resources[0].spec.replicas
    retries: 10
    delay: 5

  - name: Assert 1 Deployment exists
    assert:
      that: trustee_deployment.resources | length == 1
