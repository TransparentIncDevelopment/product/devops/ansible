---
- name: Get kubeconfig using gcloud cli
  command: gcloud container clusters get-credentials {{ gke.resource_name_prefix
    }}{{ gke.cluster_name }} --zone {{ gke.zone }}

- name: Find all persistent disks using kubectl
  command: kubectl get pv -n {{ namespace }} -o json
  register: kubectl_pv

- name: save the json to a variable as a fact
  set_fact:
    pv_json: "{{ kubectl_pv.stdout | from_json }}"

- name: Find all pv names
  vars:
    jmesquery: items[*].spec.gcePersistentDisk.pdName
  set_fact:
    pv_names: "{{ pv_json | json_query(jmesquery) }}"

- name: Check if bucket exists
  command: gsutil ls gs://{{ gke.resource_name_prefix }}{{ gke.gcp_t4_bucketname }}
  register: bucket_exists
  ignore_errors: yes

- debug:
    msg: Skipping Terraform destroy because no backend storage bucket was found
  when: bucket_exists.rc == 1

- name: Run Terraform Destroy
  community.general.terraform:
    project_path: "{{ base_save_dir }}/terraform-gke"
    workspace: "{{ t4_workspace }}"
    force_init: yes
    init_reconfigure: yes
    backend_config:
      bucket: "{{ gke.resource_name_prefix }}{{ gke.gcp_t4_bucketname }}"
    state: absent
    variables:
      gcp_project: "{{ gke.project_id }}"
      gcp_region: "{{ gke.region }}"
      gcp_zone: "{{ gke.zone }}"
      cluster_name: "{{ gke.resource_name_prefix }}{{ gke.cluster_name }}"
      cluster_version: "{{ kubernetes_cluster_version }}"
      gcp_machine_type: n1-standard-8
      gcp_min_node_count: "{{ gke.min_node_count }}"
      gcp_max_node_count: "{{ gke.max_node_count }}"
      cluster_username: ""
      cluster_password: ""
  environment:
    GOOGLE_APPLICATION_CREDENTIALS: "{{ gke_service_account_remote_file }}"
  when: bucket_exists.rc == 0

- name: Get all disks in zone after cluster deleted
  command: gcloud compute disks list --zones={{ gke.zone }}  --project={{
    gke.project_id }} --format='value(name)'
  register: all_zone_disks

- name: Find any remaining compute disks
  set_fact:
    remaining_disks: "{{ all_zone_disks.stdout_lines | intersect(pv_names) }}"

- name: Delete remaining disks that were found
  command:
    cmd: gcloud compute disks delete {{ item }} -q --zone={{ gke.zone }}
      --project={{ gke.project_id }}
  with_items: "{{ remaining_disks }}"
