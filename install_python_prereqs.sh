#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

LIGHT_RED='\033[1;31m'
LIGHT_GREEN='\033[1;32m'
RESET_COLOR='\033[0m'

echo -e "This script installs the Python3 requirements from the requirements.txt file.
It also checks that the minimum required versions of Python3 and Pip3 are installed.
This script must be run as root.\n"

# Verify this script is run as root
if [ "$EUID" -ne 0 ]
  then echo -e "${LIGHT_RED}Script was not run as root. Please run as root (sudo)${RESET_COLOR}"
  exit
fi

#Verify python3 is installed
PYTHON_TEST_COMMAND="python3"
if ! command -v "$PYTHON_TEST_COMMAND" &> /dev/null
  then
    echo -e "${LIGHT_RED}Python3 is NOT installed! ${RESET_COLOR}(tested using command: \"$PYTHON_TEST_COMMAND\")"
    exit 1
fi

#Verify python meets minimum required version (3.6+)
PYTHON_VERSION_CAPTURE=$(python3 --version | sed -n 's/^Python \([0-9]\{1,\}\(\.[0-9]\{1,\}\)\{1,\}\).*/\1/p' )
echo -e "${RESET_COLOR}$PYTHON_VERSION_CAPTURE"

PYTHON_VERSION=( ${PYTHON_VERSION_CAPTURE//./ } )
if [[ ${PYTHON_VERSION[0]} -lt 3 || ( ${PYTHON_VERSION[0]} -eq 3 && ${PYTHON_VERSION[1]} -lt 6 ) ]]
  then
    echo -e "${LIGHT_RED}Your version of Python does not meet the minimum requirements: Version 3.6+ is required.${RESET_COLOR}"
    exit 1
  else
    echo -e "${LIGHT_GREEN}Your version of Python3 meets the minimum requirements!${RESET_COLOR}"
fi

#Verify pip3 is installed
PIP3_TEST_COMMAND="pip3"
if ! command -v "$PIP3_TEST_COMMAND" &> /dev/null
  then
    echo -e "\n${LIGHT_RED}Pip3 is NOT installed! ${RESET_COLOR}(tested using command: \"$PIP3_TEST_COMMAND\")"
    exit 1
fi

#Regex verify Pip meets minimum required version (20+)
PIP_VERSION_CAPTURE=$(pip3 --version | sed -n 's/^pip \([0-9]\{1,\}\(\.[0-9]\{1,\}\)\{1,\}\).*/\1/p')
echo -e "\n${RESET_COLOR}$PIP_VERSION_CAPTURE"

PIP_VERSION=( ${PIP_VERSION_CAPTURE//./ } )
if [[ ${PIP_VERSION[0]} -lt 20 ]]
  then
    echo -e "${LIGHT_RED}Your version of Pip does not meet the minimum requirements: Version 20+ is required.${RESET_COLOR}"
    exit 1
  else
    echo -e "${LIGHT_GREEN}Your version of Pip3 meets the minimum requirements!${RESET_COLOR}"
fi

echo -e "\n"

pip3 install -r requirements.txt

echo -e "\n${LIGHT_GREEN}Python dependencies successfully installed!${RESET_COLOR}"
