from __future__ import (absolute_import, division, print_function)
from unittest import mock, TestCase
from mock import MagicMock
from ansible_collections.tpfsio.ansible.filter_plugins.toml import FilterModule
__metaclass__ = type


class TomlFilterTests(TestCase):
    def test_to_toml(self):
        filters = FilterModule().filters()
        json = {"key": "value"}
        result = filters['to_toml'](json)
        expected = "key = \"value\"\n"

        self.assertEqual(result, expected)

    def test_from_toml(self):
        filters = FilterModule().filters()
        toml = "key = \"value\""
        result = filters['from_toml'](toml)
        expected = {"key": "value"}

        self.assertEqual(result, expected)
