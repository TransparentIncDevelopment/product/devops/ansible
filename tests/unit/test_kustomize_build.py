from __future__ import (absolute_import, division, print_function)
from unittest import mock, TestCase
from mock import MagicMock
from ansible_collections.tpfsio.ansible.plugins.modules import kustomize_build
__metaclass__ = type

ANSIBLE_MODULE_NAME = 'ansible_collections.tpfsio.ansible.plugins.modules.kustomize_build.AnsibleModule'
STDOUT = 'stdout'
STDERR = 'stderr'
SUCCESS_RETURN_CODE = 0


class KustomizeBuildTests(TestCase):

    def build_fake_module(self,
                          run_command_return_value=(SUCCESS_RETURN_CODE, STDOUT, STDERR),
                          bin_path='/some/bin/folder',
                          target_path='/some/target/dir'):
        fake_module = MagicMock()
        fake_module.run_command = MagicMock(return_value=run_command_return_value)
        fake_module.get_bin_path.return_value = bin_path
        fake_module.params = {'kustomization_dir': target_path}
        return fake_module

    def test_runs_kustomize_on_target_path(self):
        fake_module = self.build_fake_module(bin_path='/fake/bin/kustomize', target_path='/fake/target')
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            kustomize_build.main()
            fake_module.run_command.assert_called_with(
                ['/fake/bin/kustomize', 'build', '.'],
                check_rc=True,
                cwd='/fake/target')

    def test_exit_json_contains_return_code_and_stderr_and_stdout(self):
        error_return_code = 5555
        fake_module = self.build_fake_module(run_command_return_value=(error_return_code, STDOUT, STDERR))
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            kustomize_build.main()
            fake_module.exit_json.assert_called_with(rc=5555, stdout=STDOUT, stderr=STDERR, changed=False)

    def test_kustomize_is_required_to_exist(self):
        fake_module = self.build_fake_module(bin_path=None)
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            kustomize_build.main()
            fake_module.get_bin_path.assert_called_with("kustomize", required=True)
