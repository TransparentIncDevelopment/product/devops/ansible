from __future__ import (absolute_import, division, print_function)
from unittest import mock, TestCase
from mock import MagicMock
from os import makedirs, path
from shutil import rmtree
from ansible.module_utils.common.text.converters import jsonify
from ansible_collections.tpfsio.ansible.plugins.modules import generate_partner_key
__metaclass__ = type

ANSIBLE_MODULE_NAME = 'ansible_collections.tpfsio.ansible.plugins.modules.generate_partner_key.AnsibleModule'
ADDRESS = '5HieAb9oqKXQbHEUAdM32GPYKY7bSUidtjWsg3dcQFVEBbHT'
STDOUT = f'Address: {ADDRESS}\nKey generated at: ./kp-{ADDRESS}'
STDERR = 'stderr'
SUCCESS_RETURN_CODE = 0
ARTIFACTS_SAVED_PATH = '/tmp/ansible-tests/generate-partner-key'


class GeneratePartnerKeyModuleTests(TestCase):

    def build_fake_module(self,
                          run_command_return_value=(SUCCESS_RETURN_CODE, STDOUT, STDERR),
                          bin_path='/some/bin/folder',
                          partner_type='Member',
                          save_path=path.join(ARTIFACTS_SAVED_PATH, "facts.d"),
                          target_path='/some/target/dir'):
        # Start with a clean slate if the path already exists
        if path.exists(save_path):
            rmtree(save_path, ignore_errors=True)

        makedirs(save_path, exist_ok=True)

        fake_module = MagicMock()
        fake_module.run_command = MagicMock(return_value=run_command_return_value)
        fake_module.get_bin_path.return_value = bin_path
        fake_module.jsonify = jsonify
        fake_module.params = {
            'facts_dir': save_path,
            'xand_dir': target_path,
            'partner_type': partner_type
        }
        return fake_module

    def test_runs_xkeygen_generate_wallet(self):
        fake_module = self.build_fake_module(
            bin_path='/fake/bin/xkeygen',
            save_path=path.join(ARTIFACTS_SAVED_PATH, "wallet", "facts.d"),
            target_path='/fake/target'
        )
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            generate_partner_key.main()
            fake_module.run_command.assert_called_with(
                ['/fake/bin/xkeygen', 'generate', 'wallet'],
                check_rc=True,
                cwd='/fake/target')

    def test_runs_xkeygen_generate_trust_key(self):
        fake_module = self.build_fake_module(
            bin_path='/fake/bin/xkeygen',
            partner_type="Trust",
            save_path=path.join(ARTIFACTS_SAVED_PATH, "trust", "facts.d"),
            target_path='/fake/target'
        )
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            generate_partner_key.main()
            fake_module.run_command.assert_called_with(
                ['/fake/bin/xkeygen', 'generate', 'trust'],
                check_rc=True,
                cwd='/fake/target')

    def test_runs_xkeygen_generate_limited_agent_key(self):
        fake_module = self.build_fake_module(
            bin_path='/fake/bin/xkeygen',
            partner_type="Limited-Agent",
            save_path=path.join(ARTIFACTS_SAVED_PATH, "limited-agent", "facts.d"),
            target_path='/fake/target'
        )
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            generate_partner_key.main()
            fake_module.run_command.assert_called_with(
                ['/fake/bin/xkeygen', 'generate', 'limited-agent'],
                check_rc=True,
                cwd='/fake/target')

    def test_exit_json_contains_return_code_and_stderr_and_jwt(self):
        error_return_code = 5555
        fake_module = self.build_fake_module(
            run_command_return_value=(error_return_code, STDOUT, STDERR),
            save_path=path.join(ARTIFACTS_SAVED_PATH, "exit_json", "facts.d"),
        )
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            generate_partner_key.main()
            fake_module.exit_json.assert_called_with(changed=True, partner_key={'address': ADDRESS})

    def test_xkeygen_is_required_to_exist(self):
        fake_module = self.build_fake_module(
            bin_path=None,
            save_path=path.join(ARTIFACTS_SAVED_PATH, "xkeygen_required", "facts.d"),
        )
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            generate_partner_key.main()
            fake_module.get_bin_path.assert_called_with("xkeygen", required=True)
